﻿//-----------------------------------------------------------------------
// <copyright file="HelloARController.cs" company="Google LLC">
//
// Copyright 2020 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// </copyright>
//-----------------------------------------------------------------------

namespace GoogleARCore.Examples.HelloAR
{
    using System.Collections;
    using GoogleARCore;
    using GoogleARCore.Examples.Common;
    using UnityEngine;
    using UnityEngine.EventSystems;

#if UNITY_EDITOR
    // Set up touch input propagation while using Instant Preview in the editor.
    using Input = InstantPreviewInput;
#endif

    public class DodgeARController : MonoBehaviour
    {
        public DepthMenu DepthMenu;
        public InstantPlacementMenu InstantPlacementMenu;
        public Camera FirstPersonCamera;

        public GameObject StageObject;

        /// The rotation in degrees need to apply to prefab when it is placed.
        private const float _prefabRotation = 180.0f;

        /// True if the app is in the process of quitting due to an ARCore connection error,
        /// otherwise false.
        private bool _isQuitting = false;

        public void Awake()
        {
            // Enable ARCore to target 60fps camera capture frame rate on supported devices.
            // Note, Application.targetFrameRate is ignored when QualitySettings.vSyncCount != 0.
            Application.targetFrameRate = 60;
        }

        public void Start()
        {
            StartCoroutine(LazyStart());
        }

        public IEnumerator LazyStart()
        {
            yield return new WaitForSeconds(1);

            if (DepthMenu != null) {
                // Show depth card window if necessary.
                DepthMenu.ConfigureDepthBeforePlacingFirstAsset();
            }
        }

        public void Update()
        {
            UpdateApplicationLifecycle();

            // If the player has not touched the screen, we are done with this update.
            Touch touch;
            if (Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Began) return;

            // Should not handle input if the player is pointing on UI.
            if (EventSystem.current.IsPointerOverGameObject(touch.fingerId)) return;

            // Raycast against the location the player touched to search for planes.
            TrackableHit hit;
            bool foundHit = false;
            if (InstantPlacementMenu.IsInstantPlacementEnabled()) {
                foundHit = Frame.RaycastInstantPlacement(touch.position.x, touch.position.y, 1.0f, out hit);
            } else {
                TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon | TrackableHitFlags.FeaturePointWithSurfaceNormal;
                foundHit = Frame.Raycast(touch.position.x, touch.position.y, raycastFilter, out hit);
            }

            if (foundHit) {
                // Use hit pose and camera pose to check if hittest is from the
                // back of the plane, if it is, no need to create the anchor.
                if ((hit.Trackable is DetectedPlane) &&
                    Vector3.Dot(FirstPersonCamera.transform.position - hit.Pose.position,
                        hit.Pose.rotation * Vector3.up) < 0) {
                    Debug.Log("Hit at back of the current DetectedPlane");
                } else {
                    // Instantiate prefab at the hit pose.
                    var gameObject = StageObject;

                    if (!gameObject.activeSelf) {
                        gameObject.SetActive(true);
                        gameObject.transform.position = hit.Pose.position;
                        gameObject.transform.rotation = hit.Pose.rotation;

                        // Compensate for the hitPose rotation facing away from the raycast (i.e. camera).
                        gameObject.transform.Rotate(0, _prefabRotation, 0, Space.Self);

                        // Create an anchor to allow ARCore to track the hitpoint as understanding of
                        // the physical world evolves.
                        var anchor = hit.Trackable.CreateAnchor(hit.Pose);

                        // Make game object a child of the anchor.
                        gameObject.transform.parent = anchor.transform;
                    } else if ((gameObject.transform.position - hit.Pose.position).magnitude <= 0.5f) {
                        // send message if stage touched
                        gameObject.SendMessage("OnTouch", hit);
                    }
                }
            }
        }

        private void UpdateApplicationLifecycle()
        {
            // Exit the app when the 'back' button is pressed.
            if (Input.GetKey(KeyCode.Escape)) {
                Application.Quit();
            }

            // Only allow the screen to sleep when not tracking.
            if (Session.Status != SessionStatus.Tracking) {
                Screen.sleepTimeout = SleepTimeout.SystemSetting;
            } else {
                Screen.sleepTimeout = SleepTimeout.NeverSleep;
            }

            if (_isQuitting) {
                return;
            }

            // Quit if ARCore was unable to connect and give Unity some time for the toast to
            // appear.
            if (Session.Status == SessionStatus.ErrorPermissionNotGranted) {
                ShowAndroidToastMessage("Camera permission is needed to run this application.");
                _isQuitting = true;
                Invoke("DoQuit", 0.5f);
            } else if (Session.Status.IsError()) {
                ShowAndroidToastMessage(
                    "ARCore encountered a problem connecting.  Please start the app again.");
                _isQuitting = true;
                Invoke("DoQuit", 0.5f);
            }
        }

        private void DoQuit()
        {
            Application.Quit();
        }

        /// <summary>
        /// Show an Android toast message.
        /// </summary>
        /// <param name="message">Message string to show in the toast.</param>
        private void ShowAndroidToastMessage(string message)
        {
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject unityActivity =
                unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

            if (unityActivity != null) {
                AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
                unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() => {
                    AndroidJavaObject toastObject =
                        toastClass.CallStatic<AndroidJavaObject>(
                            "makeText", unityActivity, message, 0);
                    toastObject.Call("show");
                }));
            }
        }
    }
}
