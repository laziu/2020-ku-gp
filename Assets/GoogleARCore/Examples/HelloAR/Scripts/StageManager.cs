﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using GoogleARCore;
using GoogleARCore.Examples.Common;
using TMPro;

#if UNITY_EDITOR
using Input = GoogleARCore.InstantPreviewInput;
#endif

public class StageManager : MonoBehaviour
{
    public TextMeshPro text;    /// Status display.
    public GameObject player;
    public GameObject bulletNormalPrefab;
    public GameObject bulletFasterPrefab;
    public GameObject bulletSlowerPrefab;

    public float bulletNormalSpeed = 0.2f;
    public float bulletFasterSpeed = 0.5f;
    public float bulletSlowerSpeed = 0.04f;

    /// Radius of rounded stage area. Player at out of stage will die. 
    public float stageRadius = 0.5f;
    /// Minimum distance between player and bullets. Player closer with bullet will die.
    public float deadDistance = 0.007f;
    /// Z-Offset from stage plane. Needed for assure those are on plane.
    public float playerZOffset = 0.02f;
    public float bulletZOffset = 0.025f;

    /// State machine status.
    enum State { Countdown, Live, Dead }
    State state = State.Dead;
    /// timer for countdown or play records.
    float timer;

    /// Bullet object management.
    const int MAX_BULLET_NUM = 2048;
    GameObject[] bullets = new GameObject[MAX_BULLET_NUM];
    ulong bulletIterationCounter;

    /// Variables for adjusting position.
    Vector3 origin, normal;

    //---- Events. ------------------------------------------------------------
    public void Start()
    {
        player.SetActive(false);
        gameObject.SetActive(false);
    }

    public void Update()
    {
        switch (state) {
            case State.Countdown:
                var count = Mathf.FloorToInt(timer);
                text.text = count > 0 ? count.ToString() : "시작!";
                break;
            case State.Live:
                text.text = $"{timer.ToString("0.0")}s";
                break;
        }
    }

    public void FixedUpdate()
    {
        RemoveDeadBullets();
        switch (state) {
            case State.Countdown: OnGameCountdown(); break;
            case State.Live: OnGameLive(); break;
            case State.Dead: OnGameDead(); break;
        }
    }

    void OnTouch(TrackableHit hit)
    {
        if (state == State.Dead) {
            TransitGameFromDeadToCountdown();
        }
    }

    //---- State machine functions. -------------------------------------------
    void OnGameDead() { }

    void TransitGameFromDeadToCountdown()
    {
        state = State.Countdown;
        timer = 3.99f;
    }

    void OnGameCountdown()
    {
        timer -= Time.fixedDeltaTime;
        if (timer < 0) {
            TransitGameFromCountdownToLive();
        }
    }

    void TransitGameFromCountdownToLive()
    {
        state = State.Live;
        player.SetActive(true);
        timer = 0;
        bulletIterationCounter = 0uL;
    }

    void OnGameLive()
    {
        timer += Time.fixedDeltaTime;

        // variables for adjusting position of player and bullets to be placed on stage properly
        origin = gameObject.transform.position;
        normal = gameObject.transform.up.normalized;

        // Raycast screen center and move player to the position
        TrackableHit hit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon | TrackableHitFlags.FeaturePointWithSurfaceNormal;
        bool foundHit = Frame.Raycast(Screen.width * 0.5f, Screen.height * 0.5f, raycastFilter, out hit);

        if (foundHit) {
            player.transform.position = AdjustPositionOnStage(hit.Pose.position, playerZOffset);
        }

        // check player is out of stage
        if (IsOutOfStage(player.transform.position)) {
            TransitGameFromLiveToDead();
            return;
        }

        // Create new bullets
        CreateNewBullets(player.transform.position);

        // check player collides with a bullet
        if (WhichBulletCollideWithPlayer(player.transform.position) >= 0) {
            TransitGameFromLiveToDead();
            return;
        }
    }

    void TransitGameFromLiveToDead()
    {
        state = State.Dead;
        player.SetActive(false);
        text.text = $"사    망\n{timer.ToString("0.0")}s\n터치해서 재시작";
    }

    //---- Game management. -------------------------------------------------
    Vector3 ProjectToStage(Vector3 target) => target + Vector3.Dot(normal, origin - target) * normal;

    Vector3 AdjustPositionOnStage(Vector3 target, float offset) => ProjectToStage(target) + normal * offset;

    bool IsOutOfStage(Vector3 target) => (target - origin).magnitude > stageRadius;

    void RemoveDeadBullets()
    {
        for (int i = 0; i < MAX_BULLET_NUM; i++) {
            var bullet = bullets[i];
            if (bullet == null) continue;

            if (IsOutOfStage(bullet.transform.position)) {
                bullets[i] = null;
                Destroy(bullet);
            }
        }
    }

    void CreateNewBullets(Vector3 target)
    {
        bulletIterationCounter++;
        if (bulletIterationCounter % 3 != 0) return;

        int idx = FindEmptyBulletIndex();
        if (idx < 0) return;

        (GameObject bulletPrefab, float bulletSpeed) = NextBulletData();
        var bulletPosition = RandomBulletStartPosition();
        var targetPosition = AdjustPositionOnStage(target, bulletZOffset);
        var bulletDirection = (targetPosition - bulletPosition).normalized;

        var bullet = Instantiate(bulletPrefab, bulletPosition, Quaternion.identity);
        bullet.GetComponent<Rigidbody>().velocity = bulletSpeed * bulletDirection;

        bullets[idx] = bullet;
    }

    int FindEmptyBulletIndex()
    {
        for (int i = 0; i < MAX_BULLET_NUM; i++) {
            if (bullets[i] == null)
                return i;
        }
        return -1;
    }

    (GameObject, float) NextBulletData()
    {
        bulletIterationCounter++;

        if (bulletIterationCounter >= 2000 && bulletIterationCounter % 441 == 0) {
            return (bulletSlowerPrefab, bulletSlowerSpeed);
        } else if (bulletIterationCounter >= 1000 && bulletIterationCounter % 30 == 0) {
            return (bulletFasterPrefab, bulletFasterSpeed);
        } else {
            return (bulletNormalPrefab, bulletNormalSpeed);
        }
    }

    Vector3 RandomBulletStartPosition()
    {
        float angle = Random.Range(0f, 2f * Mathf.PI);
        var position = origin + (stageRadius - 0.005f) * new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle));
        return AdjustPositionOnStage(position, bulletZOffset);
    }

    int WhichBulletCollideWithPlayer(Vector3 playerPosition)
    {
        for (int i = 0; i < MAX_BULLET_NUM; i++) {
            var bullet = bullets[i];
            if ((bullet.transform.position - playerPosition).magnitude < deadDistance) {
                return i;
            }
        }
        return -1;
    }
}
